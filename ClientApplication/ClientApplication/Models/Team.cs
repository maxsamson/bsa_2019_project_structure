﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientApplication.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created_At { get; set; }
        public List<User> Users { get; set; }
        public List<Project> Projects { get; set; }

        public Team()
        {
            Users = new List<User>();
            Projects = new List<Project>();
        }
    }
}
