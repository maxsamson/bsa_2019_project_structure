﻿using ClientApplication.Models;

namespace Project_Structure.Shared.SelectionModels
{
    public class ProjectStruct
    {
        public Project Project { get; set; }
        public Task LongestTask { get; set; }
        public Task ShortestTask { get; set; }
        public int UsersCount { get; set; }
    }
}
