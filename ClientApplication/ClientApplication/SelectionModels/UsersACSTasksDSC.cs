﻿using ClientApplication.Models;
using System.Collections.Generic;

namespace Project_Structure.Shared.SelectionModels
{
    public class UsersACSTasksDSC
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
