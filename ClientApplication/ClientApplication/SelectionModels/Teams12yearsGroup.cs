﻿using ClientApplication.Models;
using System.Collections.Generic;

namespace Project_Structure.Shared.SelectionModels
{
    public class Teams12yearsGroup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
