﻿using ClientApplication.Models;
using System.Collections.Generic;

namespace Project_Structure.Shared.SelectionModels
{
    public class UserStruct
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public List<Task> NotFinishedCanceledTasks { get; set; }
        public Task MaxDurationTask { get; set; }
    }
}
