﻿using ClientApplication.Models;

namespace Project_Structure.Shared.SelectionModels
{
    public class TasksCount
    {
        public Project Project { get; set; }
        public int Count { get; set; }
    }
}
