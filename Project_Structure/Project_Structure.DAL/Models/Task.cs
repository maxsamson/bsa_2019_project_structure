﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Models
{
    public enum State { Created, Started, Finished, Canceled }

    public class Task : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Created_At { get; set; }
        public DateTime Finished_At { get; set; }
        public State State { get; set; }
        public Project Project { get; set; }
        public User Performer { get; set; }
    }
}
