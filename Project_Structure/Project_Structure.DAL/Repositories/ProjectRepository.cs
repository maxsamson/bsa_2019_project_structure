﻿using DAL.DBInfrastructure;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class ProjectRepository : BaseRepository<Project>
    {
        public ProjectRepository(DBContext dBContext) : base(dBContext)
        {

        }
    }
}
