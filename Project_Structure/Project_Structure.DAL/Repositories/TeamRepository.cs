﻿using DAL.DBInfrastructure;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class TeamRepository : BaseRepository<Team>
    {
        public TeamRepository(DBContext dBContext) : base(dBContext)
        {

        }
    }
}
