﻿using DAL.DBInfrastructure;
using DAL.Repositories;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Mapping;
using Project_Structure.BLL.Services;
using Project_Structure.BLL.Validators;
using Project_Structure.Shared.DTO;

namespace Project_Structure
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddScoped<IProjectService, ProjectService>();
            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<ITeamService, TeamService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ISelectionService, SelectionService>();

            services.AddTransient<AbstractValidator<ProjectDTO>, ProjectValidator>();
            services.AddTransient<AbstractValidator<TaskDTO>, TaskValidator>();
            services.AddTransient<AbstractValidator<TeamDTO>, TeamValidator>();
            services.AddTransient<AbstractValidator<UserDTO>, UserValidator>();

            services.AddScoped<ProjectRepository>();
            services.AddScoped<TaskRepository>();
            services.AddScoped<TeamRepository>();
            services.AddScoped<UserRepository>();

            services.AddSingleton<DBContext>();

            services.AddTransient<IMapper, Mapper>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
