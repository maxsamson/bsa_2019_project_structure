﻿using System;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;

namespace Project_Structure.Controllers
{
    [Route("api/Selections")]
    [ApiController]
    public class SelectionController : ControllerBase
    {
        readonly ISelectionService service;

        public SelectionController(ISelectionService selectionService)
        {
            service = selectionService;
        }

        // GET: api/Selections/CreateHierarhy
        [Route("CreateHierarhy")]
        [HttpGet]
        public IActionResult CreateHierarhy()
        {
            try
            {
                return Ok(service.CreateHierarhy());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/TasksCount
        [Route("TasksCount/{id:int}")]
        [HttpGet]
        public IActionResult TasksCount(int id)
        {
            try
            {
                return Ok(service.TasksCount(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/TasksWithNameLess45Symbols
        [Route("TasksWithNameLess45Symbols/{id:int}")]
        [HttpGet]
        public IActionResult TasksWithNameLess45Symbols(int id)
        {
            try
            {
                return Ok(service.TasksWithNameLess45Symbols(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/TasksFinished2019
        [Route("TasksFinished2019/{id:int}")]
        [HttpGet]
        public IActionResult TasksFinished2019(int id)
        {
            try
            {
                return Ok(service.TasksFinished2019(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/Teams12yearsGroup
        [Route("Teams12yearsGroup")]
        [HttpGet]
        public IActionResult Teams12yearsGroup()
        {
            try
            {
                return Ok(service.Teams12yearsGroup());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/UsersACSTasksDSC
        [Route("UsersACSTasksDSC")]
        [HttpGet]
        public IActionResult UsersACSTasksDSC()
        {
            try
            {
                return Ok(service.UsersACSTasksDSC());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/UserStruct
        [Route("UserStruct/{id:int}")]
        [HttpGet]
        public IActionResult UserStruct(int id)
        {
            try
            {
                return Ok(service.UserStruct(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Selections/ProjectStruct
        [Route("ProjectStruct/{id:int}")]
        [HttpGet]
        public IActionResult ProjectStruct(int id)
        {
            try
            {
                return Ok(service.ProjectStruct(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}