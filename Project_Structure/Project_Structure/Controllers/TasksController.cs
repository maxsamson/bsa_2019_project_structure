﻿using System;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;

namespace Project_Structure.Controllers
{
    [Route("api/Tasks")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        readonly ITaskService service;

        public TasksController(ITaskService taskService)
        {
            service = taskService;
        }

        // GET: api/Tasks
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(service.GetAll());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                return Ok(service.GetById(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST: v1/api/Tasks
        [HttpPost]
        public IActionResult Post([FromBody]TaskDTO task)
        {
            try
            {
                service.Create(task);
                return Ok();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: v1/api/Tasks/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] TaskDTO task)
        {
            try
            {
                service.Update(id, task);
                return Ok();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: v1/api/Tasks/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                service.DeleteById(id);
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // DELETE: v1/api/Tasks
        [HttpDelete]
        public IActionResult Delete([FromBody] TaskDTO task)
        {
            try
            {
                service.Delete(task);
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}