﻿using System;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;

namespace Project_Structure.Controllers
{
    [Route("api/Teams")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        readonly ITeamService service;

        public TeamsController(ITeamService teamService)
        {
            service = teamService;
        }

        // GET: api/Teams
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(service.GetAll());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/Teams/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                return Ok(service.GetById(id));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST: v1/api/Teams
        [HttpPost]
        public IActionResult Post([FromBody]TeamDTO team)
        {
            try
            {
                service.Create(team);
                return Ok();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: v1/api/Teams/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] TeamDTO team)
        {
            try
            {
                service.Update(id, team);
                return Ok();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: v1/api/Teams/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                service.DeleteById(id);
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // DELETE: v1/api/Teams
        [HttpDelete]
        public IActionResult Delete([FromBody] TeamDTO team)
        {
            try
            {
                service.Delete(team);
                return NoContent();
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}