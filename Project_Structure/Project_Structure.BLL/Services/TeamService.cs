﻿using DAL.Repositories;
using FluentValidation;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;
using System;
using System.Collections.Generic;

namespace Project_Structure.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly TeamRepository repository;
        private readonly IMapper mapper;
        private readonly AbstractValidator<TeamDTO> validator;

        public TeamService(TeamRepository teamRepository, IMapper mapper, AbstractValidator<TeamDTO> validator)
        {
            this.repository = teamRepository;
            this.mapper = mapper;
            this.validator = validator;
        }

        public List<TeamDTO> GetAll()
        {
            var result = new List<TeamDTO>();
            foreach (var item in repository.GetAll())
            {
                result.Add(mapper.MapTeam(item));
            }
            return result;
        }

        public TeamDTO GetById(int id)
        {
            return mapper.MapTeam(repository.GetById(id));
        }

        public int Create(TeamDTO team)
        {
            var validationResult = validator.Validate(team);
            if (validationResult.IsValid)
                return repository.Create(mapper.MapTeam(team));
            else
                throw new ValidationException(validationResult.Errors);
        }

        public void Update(int id, TeamDTO team)
        {
            var validationResult = validator.Validate(team);
            if (!validationResult.IsValid)
                throw new ValidationException(validationResult.Errors);
            try
            {
                repository.Update(id, mapper.MapTeam(team));
            }
            catch (ArgumentNullException)
            {
                throw new NotFoundException();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(TeamDTO team)
        {
            repository.Delete(mapper.MapTeam(team));
        }

        public void DeleteById(int id)
        {
            repository.DeleteById(id);
        }
    }
}
