﻿using DAL.Repositories;
using FluentValidation;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;
using System;
using System.Collections.Generic;

namespace Project_Structure.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly UserRepository repository;
        private readonly IMapper mapper;
        private readonly AbstractValidator<UserDTO> validator;

        public UserService(UserRepository userRepository, IMapper mapper, AbstractValidator<UserDTO> validator)
        {
            this.repository = userRepository;
            this.mapper = mapper;
            this.validator = validator;
        }

        public List<UserDTO> GetAll()
        {
            var result = new List<UserDTO>();
            foreach (var item in repository.GetAll())
            {
                result.Add(mapper.MapUser(item));
            }
            return result;
        }

        public UserDTO GetById(int id)
        {
            return mapper.MapUser(repository.GetById(id));
        }

        public int Create(UserDTO user)
        {
            var validationResult = validator.Validate(user);
            if (validationResult.IsValid)
                return repository.Create(mapper.MapUser(user));
            else
                throw new ValidationException(validationResult.Errors);
        }

        public void Update(int id, UserDTO user)
        {
            var validationResult = validator.Validate(user);
            if (!validationResult.IsValid)
                throw new ValidationException(validationResult.Errors);
            try
            {
                repository.Update(id, mapper.MapUser(user));
            }
            catch (ArgumentNullException)
            {
                throw new NotFoundException();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(UserDTO user)
        {
            repository.Delete(mapper.MapUser(user));
        }

        public void DeleteById(int id)
        {
            repository.DeleteById(id);
        }
    }
}
