﻿using DAL.Repositories;
using FluentValidation;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;
using System;
using System.Collections.Generic;

namespace Project_Structure.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly TaskRepository repository;
        private readonly IMapper mapper;
        private readonly AbstractValidator<TaskDTO> validator;

        public TaskService(TaskRepository taskRepository, IMapper mapper, AbstractValidator<TaskDTO> validator)
        {
            this.repository = taskRepository;
            this.mapper = mapper;
            this.validator = validator;
        }

        public List<TaskDTO> GetAll()
        {
            var result = new List<TaskDTO>();
            foreach (var item in repository.GetAll())
            {
                result.Add(mapper.MapTask(item));
            }
            return result;
        }

        public TaskDTO GetById(int id)
        {
            return mapper.MapTask(repository.GetById(id));
        }

        public int Create(TaskDTO task)
        {
            var validationResult = validator.Validate(task);
            if (validationResult.IsValid)
                return repository.Create(mapper.MapTask(task));
            else
                throw new ValidationException(validationResult.Errors);
        }

        public void Update(int id, TaskDTO task)
        {
            var validationResult = validator.Validate(task);
            if (!validationResult.IsValid)
                throw new ValidationException(validationResult.Errors);
            try
            {
                repository.Update(id, mapper.MapTask(task));
            }
            catch (ArgumentNullException)
            {
                throw new NotFoundException();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(TaskDTO task)
        {
            repository.Delete(mapper.MapTask(task));
        }

        public void DeleteById(int id)
        {
            repository.DeleteById(id);
        }
    }
}
