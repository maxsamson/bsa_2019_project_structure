﻿using FluentValidation;
using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.BLL.Validators
{
    public class UserValidator : AbstractValidator<UserDTO>
    {
        public UserValidator()
        {
            RuleFor(p => p.First_Name.Length).NotNull().NotEmpty().GreaterThanOrEqualTo(1);
            RuleFor(p => p.Last_Name.Length).NotNull().NotEmpty().GreaterThanOrEqualTo(1);
            RuleFor(p => p.Email).EmailAddress();
            RuleFor(p => p.Birthday).LessThan(DateTime.Now);
            RuleFor(p => p.Registered_At).LessThanOrEqualTo(DateTime.Now);
        }
    }
}
